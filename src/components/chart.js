import React from'react';
import _ from'lodash';
import {Sparklines, SparklinesLine, SparklinesReferenceLine} from 'react-sparklines';

export default function(props) {
    
   const  average=(num)=>{
        return _.round(_.sum(num)/num.length)
    }
    
    return (
        <div className="container">
            <Sparklines height={120} width={180} data={props.data}>
                <SparklinesLine color={props.color} />
                 <SparklinesReferenceLine type="avg" />
            </Sparklines>
            <div>{average(props.data)} {props.units}</div>
        </div>
        
    );
}