import axios from 'axios';

const API_KEY = "fe032a86748f1da94284a17eb5176810";

const ROOT_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;
export const FETCH_WEATHER = "FETCH_WEATHER";

export default function featchWeather(city,country){
    const url = `${ROOT_URL}&q=${city},${country}`;
    const request = axios.get(url);
    
    return{
        type: FETCH_WEATHER,
        payload: request
    };
}