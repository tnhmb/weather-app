import {FETCH_WEATHER} from '../actions/index';

export default function (state= [], action){
    if(action.error){
        action.type = "ERROR_HANDLER";
    }
    switch(action.type){
        case FETCH_WEATHER:
            return [action.payload.data, ...state]; // equivilant to state.concat([action.payload.datat])
        case "ERROR_HANDLER": {
            alert("City not found.");
            return state; 
        }
        
    }
    
    return state;
}