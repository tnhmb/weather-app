import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import fetchWeather from '../actions/index';



class SearchBar extends Component {
    constructor(props){
        super(props);
        
        this.state = {term: "", country: ""};
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.renderSelectOptions = this.renderSelectOptions.bind(this)
    }
    renderSelectOptions = (country) => {
        return <option key={country.code} value={country.code} >{country.name}</option>;
    }
    
    
    render (){
        return(
            <nav className="navbar navbar-dark bg-dark d-flex justify-content-center">
                <form onSubmit={this.onFormSubmit} className="input-group ">
                    <input onChange={this.onInputChange} value={this.state.term} className="form-control my-2 my-sm-0 " type="search" placeholder="Search" aria-label="Search" />
                    <select value={this.state.country} onChange={this.handleChange} className="form-control">
                      <option>Select</option>
                         {this.props.country.map(this.renderSelectOptions)}
                    </select>
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </nav>
        );
    }
    onInputChange = (event) =>{
        console.log(event.target.value);
        this.setState({term: event.target.value }) ;
    }
     handleChange(event) {
    this.setState({country: event.target.value});
  }
    onFormSubmit = (event) =>{
        event.preventDefault();
        this.props.fetchWeather(this.state.term, this.state.country);
        this.setState({term: '', country:''}) ;

    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchWeather}, dispatch)
}
function mapStateToProps({country}){
    return { country }; // {weather } === {weahter: weather}
}
export default connect (mapStateToProps, mapDispatchToProps)(SearchBar);