import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import fetchWeather from '../actions/index';



class DropMenu extends Component {
    constructor(props){
        super(props);
        
        this.state = {term: ""};
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }
    
    render (){
        return(
            <nav className="navbar navbar-dark bg-dark d-flex justify-content-center">
                <form onSubmit={this.onFormSubmit} className="input-group ">
                    <input onChange={this.onInputChange} value={this.state.term} className="form-control my-2 my-sm-0 " type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </nav>
        );
    }
    onInputChange = (event) =>{
        console.log(event.target.value);
        this.setState({term: event.target.value}) ;
    }
    
    onFormSubmit = (event) =>{
        event.preventDefault();
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''}) ;

    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchWeather}, dispatch)
}

export default connect (null, mapDispatchToProps)(SearchBar);