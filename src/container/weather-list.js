import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Sparklines, SparklinesLine} from 'react-sparklines';
import Chart from '../components/chart';
import GoogleMap from '../components/google-maps';

class WeatherList extends Component{
    constructor(props){
        super(props);
        
        this.renderWeather = this.renderWeather.bind(this);
    }
    renderWeather(cityData){
        if(cityData.cod === "404"){
            alert("city not found");
            return ;
        }
        const name = cityData.city.name;
        const temps = cityData.list.map(weather => weather.main.temp - 273.15 );
        const humids = cityData.list.map(weather => weather.main.humidity); 
        const pressure = cityData.list.map(weather => weather.main.pressure); 
        const {lon, lat} = cityData.city.coord;
        
        return(
            <tr className="main-table" key={name}>
                <td><GoogleMap lon = {lon} lat={lat} />></td>
                <td>
                     <Chart data={temps} color="red" units="C" />
                </td>
                <td>
                     <Chart data={humids} color="blue" units="%" />
                </td>
                <td>
                     <Chart data={pressure} color="black" units="hPA" />
                </td>
            </tr>
        );
        
    }
    
    render(){
        return(
            <table className="table table-light table-hover ">
              <thead className="thead-dark">
                <tr>
                    <th scope="col">City</th>
                    <th scope="col">Temperature (C)</th>
                    <th scope="col">Humidty (%)</th>
                    <th scope="col">Pressure (hPA)</th>
                </tr>
              </thead>
              <tbody>
                {this.props.weather.map(this.renderWeather)}
              </tbody>
            </table>
        );
    }
}

function mapStateToProps({weather}){
    return { weather }; // {weather } === {weahter: weather}
}
export default connect(mapStateToProps)(WeatherList);